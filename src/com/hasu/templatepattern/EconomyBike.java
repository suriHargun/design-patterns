package com.hasu.templatepattern;

public class EconomyBike extends Vehicle{

	public EconomyBike() {
		super();
		System.out.println("This is Economy Bike features");
	}

	@Override
	public void additionalFeatures() {
		
		System.out.println("Economy vehicle no additional features are provided");
	}

}
