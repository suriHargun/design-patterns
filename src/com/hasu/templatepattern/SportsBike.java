package com.hasu.templatepattern;

public class SportsBike extends Vehicle {

	public SportsBike() {
		super();
		System.out.println("This is Sorts Bike Features");
	}

	@Override
	public void additionalFeatures() {
		System.out.println("Adding some more additional features");
		improvedBreaking();
		improvedSpeed();
		
	}
	
	
	public void improvedBreaking(){
		System.out.println("Adding disk break additionally");
	}
	
	public void improvedSpeed(){
		System.out.println("Improving speed by adding Boosters");
	}
	

}
