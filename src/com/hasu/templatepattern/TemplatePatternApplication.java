package com.hasu.templatepattern;

public class TemplatePatternApplication {

	public static void main(String[] args) {
		
		System.out.println("Look for bike");
		
		Vehicle vehicleOne = new EconomyBike();
		vehicleOne.features();
		
		Vehicle vehicleTwo = new SportsBike();
		vehicleTwo.features();

	}

}
