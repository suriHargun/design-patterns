package com.hasu.templatepattern;

public abstract class Vehicle {

	// Template method of template class
	public void features() {
		horn();
		breaking();
		wheels();
		additionalFeatures();

	}

	// Method already defined in the Template class basically used to perform
	// some features of algorithm
	private void horn() {
		System.out.println("Basic horn is provided");
	}

	// Method already defined in the Template class basically used to perform
	// some features of algorithm
	private void breaking() {
		System.out.println("Basic Drum break is provided");
	}

	// Method already defined in the Template class basically used to perform
	// some features of algorithm
	private void wheels() {
		System.out.println("Basic 2 wheeler vehicle");
	}

	// Method we need to define in base class here we will add other feature of
	// algorithm, ie the class extend the Template class will have rights to
	// perform additioonal features
	public abstract void additionalFeatures();
}
