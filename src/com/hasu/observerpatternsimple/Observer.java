package com.hasu.observerpatternsimple;

public class Observer {
	
	public void update(){
		System.out.println("Flag value changed in Subject");
	}

}
