package com.hasu.observerpatternsimple;

public class ObserverPatternApplication {

	public static void main(String[] args) {
		System.out.println("Observer Pattern Example ** ");
		
		System.out.println("Now register 3 observer");
		//Create 3 observers
		Observer ob1 = new Observer();
		Observer ob2 = new Observer();
		Observer ob3 = new Observer();
		
		//Register these observers on subject
		SubjectImplementation mainSubject = new SubjectImplementation();
		mainSubject.register(ob1);
		mainSubject.register(ob2);
		mainSubject.register(ob3);
		
		System.out.println("Set some value in Subject");
		//set the value of flag
		mainSubject.setFlag(1);
		
		System.out.println("Now unregister 1 observer");
		
		//Unregister 1 Observer on Subject
		mainSubject.unregister(ob1);
		
		System.out.println("Set some value in Subject");
		//set the value of the flag
		mainSubject.setFlag(2);
		
	}

}
