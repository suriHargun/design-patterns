package com.hasu.observerpatternsimple;

import java.util.ArrayList;
import java.util.List;

public class SubjectImplementation implements Subject {
	
	List<Observer> listOfObserver = new ArrayList<Observer>();
	private int flag;

	public int getFlag() {
		return flag;
	}

	public void setFlag(int flag) {
		this.flag = flag;
		notifyObserver();
	}

	@Override
	public void register(Observer observer) {
		listOfObserver.add(observer);
	}

	@Override
	public void unregister(Observer observer) {
		listOfObserver.remove(observer);

	}

	@Override
	public void notifyObserver() {
		listOfObserver.forEach(observer -> observer.update());
	}

}
