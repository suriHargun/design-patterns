package com.hasu.observerpatterncomplex;

public class Arun implements Subscriber {

	@Override
	public void update(String message) {
		System.out.println(this.getClass().getSimpleName() +" " +message);
	}

}
