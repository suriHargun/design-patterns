package com.hasu.observerpatterncomplex;

public interface Publisher {

	public void subscribe(Subscriber subscriber);
	public void unSubscribe(Subscriber subscriber);
	public void notify(String value);
}
