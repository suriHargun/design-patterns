package com.hasu.observerpatterncomplex;

public class Prithvi implements Subscriber {

	@Override
	public void update(String message) {
		System.out.println(this.getClass().getSimpleName() +" " +message);
	}

}
