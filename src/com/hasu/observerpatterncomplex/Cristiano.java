package com.hasu.observerpatterncomplex;

import java.util.ArrayList;
import java.util.List;

public class Cristiano implements Publisher{
	
	List<Subscriber> subscribers = new ArrayList<Subscriber>();
	private String message;

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
		notify(message);
	}

	@Override
	public void subscribe(Subscriber subscriber) {
		subscribers.add(subscriber);
		
	}

	@Override
	public void unSubscribe(Subscriber subscriber) {
		subscribers.remove(subscriber);
		
	}

	@Override
	public void notify(String message) {
		
		subscribers.forEach(subscriber -> {subscriber.update(message);});
		
	}

}
