package com.hasu.observerpatterncomplex;

public class PublisherSubscriber {

	public static void main(String[] args) {
		
		//Create Instance of Subscribers
		Arun arun = new Arun();
		Hargun hargun = new Hargun();
		Prithvi prithvi = new Prithvi();
		Rohit rohit = new Rohit();
		
		//Create Instance of Publishers
		Virat virat = new Virat();
		Cristiano  cristiano = new Cristiano();
		
		//Subscribe Subscriber onto publisher
		virat.subscribe(arun);
		virat.subscribe(hargun);
		
		cristiano.subscribe(prithvi);
		cristiano.subscribe(rohit);
		
		//Send message of Publisher to Subscriber
		virat.setMessage(" :Virat Kohli Tweeted : Heading Toward 100th Century !!");
		cristiano.setMessage(" :Cristiano Tweeted : GOAT For a reason !!");

		//UnSubscribe Subscriber from publisher
		virat.unSubscribe(arun);
		cristiano.unSubscribe(rohit);
		
		//Send message of Publisher to Subscriber
		virat.setMessage(" :Virat Kohli Tweeted : I will break sachin's record 1 day !!");
		cristiano.setMessage(" :Cristiano Tweeted : Messi should learn from me !!");
	}

}
