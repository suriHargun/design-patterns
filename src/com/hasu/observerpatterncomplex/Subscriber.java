package com.hasu.observerpatterncomplex;

public interface Subscriber {

	public void update(String value);

}
